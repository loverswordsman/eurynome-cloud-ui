declare function translater(template: string, replacements: Record<string, string>): string;
declare const _default: {
    translate: (string | typeof translater)[];
};
export default _default;
//# sourceMappingURL=custom.d.ts.map