import { Vue } from 'vue-property-decorator';
export default class HPanelTextField extends Vue {
    inputValue: string;
    readonly label?: string;
    readonly tooltip?: string;
    readonly disabled?: boolean;
}
//# sourceMappingURL=HPanelTextField.vue?rollup-plugin-vue=script.d.ts.map