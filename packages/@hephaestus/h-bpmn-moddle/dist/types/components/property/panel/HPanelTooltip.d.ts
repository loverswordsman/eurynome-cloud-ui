import { Vue } from 'vue-property-decorator';
export default class HPanelTooltip extends Vue {
    readonly content: string;
}
//# sourceMappingURL=HPanelTooltip.vue?rollup-plugin-vue=script.d.ts.map