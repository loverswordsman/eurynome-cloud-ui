import Vue from 'vue';
import HBpmnModdle from './HBpmnModdle.vue';
declare const HBpmnModdleInstall: {
    install(vue: typeof Vue): void;
};
export default HBpmnModdle;
export { HBpmnModdleInstall as HBpmnModdle };
//# sourceMappingURL=index.d.ts.map