import CamundaModdleExtension from './extension';

export default {
    __init__: ['CamundaModdleExtension'],
    ActivitiModdleExtension: ['type', CamundaModdleExtension],
};
