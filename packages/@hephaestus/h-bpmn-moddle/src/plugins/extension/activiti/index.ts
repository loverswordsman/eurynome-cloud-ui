/*
 * @author igdianov
 * address https://github.com/igdianov/activiti-bpmn-moddle
 * */

import ActivitiModdleExtension from './extension';

export default {
    __init__: ['ActivitiModdleExtension'],
    ActivitiModdleExtension: ['type', ActivitiModdleExtension],
};
