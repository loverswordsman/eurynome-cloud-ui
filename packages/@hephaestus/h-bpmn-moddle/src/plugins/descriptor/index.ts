import CamundaModdleDescriptor from './camunda.json';
import ActivitiModdleDescriptor from './activiti.json';
import FlowableModdleDescriptor from './flowable.json';

export { CamundaModdleDescriptor, ActivitiModdleDescriptor, FlowableModdleDescriptor };
