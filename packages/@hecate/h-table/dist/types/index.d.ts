import Vue from 'vue';
import HTable from './HTable.vue';
declare const HTableInstall: {
    install: (vue: typeof Vue) => void;
};
export { HTableInstall as HTableComponent };
export default HTable;
//# sourceMappingURL=index.d.ts.map