declare module '@hecate/utils';

interface RSAPair {
    publicKey: string;
    privateKey: string;
}
