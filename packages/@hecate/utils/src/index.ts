import * as library from './lib';
import * as localize from './locales';

export { library, localize };
